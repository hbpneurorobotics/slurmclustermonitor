/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/
var nexpect = require('nexpect');
var _ = require('lodash');
var moment = require('moment');

var second = 1000;
var minute = second * 60;

var slurmInfo = {};


// double -t needed: see http://stackoverflow.com/questions/7114990/pseudo-terminal-will-not-be-allocated-because-stdin-is-not-a-terminal
var SSH_COMMAND = 'ssh -t -t bbpnrsoa@bbpviz1.cscs.ch -f';
var SINFO_COMMAND = 'sinfo -o "%20P | %5a | %15C | %15F | %G"';
var SINFO_RESERVED_COMMAND = 'sinfo -o "%20P | %A" -t reserved';
slurmInfo.sinfoCommand = SSH_COMMAND + ' ' + SINFO_COMMAND + ' && ' + SINFO_RESERVED_COMMAND;

slurmInfo.reset = function() {
  slurmInfo.sinfoText = null;
  slurmInfo.latestSinfo = null;
  slurmInfo.latestTime = moment();
};

slurmInfo.sinfo = function(callback) {
  callback = callback ? callback : _.noop;
  nexpect.spawn(slurmInfo.sinfoCommand)
    .run(function(err, stdout, exitCode) {
      slurmInfo.reset();
      if (err) {
        console.error('error when running ssh:',err);
        callback(err);
      }
      else {
        slurmInfo.sinfoText = stdout;
        try {
          var parsed = slurmInfo.parseInfo(stdout);
          slurmInfo.latestSinfo = parsed;
          callback(null, parsed);
        }
        catch (error) {
          console.error('error during parsing: ', error);
          slurmInfo.reset();
          callback(error);
        }
      }
    });
};

slurmInfo.parseInfo = function(infoArr) {
  // there can be some ssh junk before sinfo output
  var idxSinfoBegin = _.findIndex(infoArr, function(line) {
    return (line.indexOf('PARTITION') > -1);
  });
  //get start of reserved output
  var idxSinfoReserved = _.findIndex(infoArr, function (line) {
    return (line.indexOf('PARTITION') > -1);
  }, idxSinfoBegin + 1);

  if (idxSinfoBegin === -1 || idxSinfoReserved === -1) {
    throw new ReferenceError("sinfo output not found");
  }

  // remove all lines before the header line, and sinfo reserved output
  var availableArr = infoArr.slice(idxSinfoBegin + 1, idxSinfoReserved);
  //take sinfo reserved output
  var reservedArr = infoArr.slice(idxSinfoReserved + 1);

  var parseInt10 = _.partial(parseInt, _, 10);
  var gpuRegex = /\s*gpu:(\d*)/;

  //build hash table with reserved idle instances per partition
  var reserved = _(reservedArr)
    .map(function (partition) {
      var p = partition.split('|');
      return {
        name: p[0].trim(),
        idle: parseInt10(p[1].split('/')[1])
      };
    })
    .keyBy('name')
    .value();

  var ret = _.map(availableArr, function(partition) {
    var p = partition.split('|');
    var gpu = p[4].match(gpuRegex);
    var nodes = _.map(p[3].split('/'), parseInt10);
    var name = p[0].trim();
    return {
      name: name,
      available: (p[1].indexOf('up') > -1),
      cpus: _.map(p[2].split('/'), parseInt10),
      nodes: nodes,
      gpus: gpu ? parseInt10(gpu[1]) : 0,
      free: nodes[1] - reserved[name].idle
    };
  });
  return ret;
};

slurmInfo.latestSinfo = null;

slurmInfo.startFetchSinfo = function() {
  slurmInfo.reset();
  slurmInfo.sinfo();
  slurmInfo.sinfoIntervalId = setInterval(slurmInfo.sinfo, minute);
};

module.exports = slurmInfo;

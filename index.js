/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/
var nexpect = require('nexpect');
var _ = require('lodash');
var slurmInfo = require('./utils/slurmInfo');
var path = require('path');

var express = require('express');
var app = express();


slurmInfo.startFetchSinfo();

app.get('/sinfo', function(req, res) {
  res.json({
    sinfoCommand: slurmInfo.sinfoCommand,
    sinfoText: slurmInfo.sinfoText,
    sinfoTime: slurmInfo.latestTime
  });
});

app.get('/', function(req, res) {
  res.redirect('/html/index.html');
});

app.get('/api/v1/partitions', function(req, res) {
  res.json(_.map(slurmInfo.latestSinfo, 'name'));
});

app.get('/api/v1/partitions/:name', function(req, res) {
  var partition = _.find(slurmInfo.latestSinfo, {'name' : req.params.name});
  if (partition) {
    res.json(partition);
  }
  else {
    res.status(404).send('No partition with this name');
  }
});

app.get('/api/v1/sinfo', function(req, res) {
  res.json(slurmInfo.latestSinfo);
});

app.get('/health', function(req, res) {
  res.status(200).send();
});

app.use(express.static('public'));

function startServer(port) {
  app.listen(port, function() {
    console.log('Listening on port '+port);
  });
  return;
}


if (!module.parent) {
  var port = 3000;
  if (process.argv.length > 2) {
    port = parseInt(process.argv[2], 10);
    if (_.isNaN(port)) {
      console.error('argument for port is not a number:',process.argv[2]);
      process.exit(1);
    }
  }
  startServer(port);
} else {
  module.exports = exports = nano = startServer;
}

/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/
/*global $, document, window, BbpOidcClient*/
(function() {
  'use strict';

  var init = function() {
    $(document).ready(retrieveSlurmInfo);
  }

  // authenticate() function configures the BbpOidcClient and
  // retrieves the current user token.
  //
  // If the user is not authenticated, it will redirect to the HBP auth server
  // and use an OpenID connect implicit flow to retrieve an user access token.
  var authenticate = function() {
    // Setup OpenID connect authentication using the clientId provided
    // in the HBP OIDC client page.
    // https://collab.humanbrainproject.eu/#/collab/54/nav/1051
    var oidcClient = new BbpOidcClient({
      clientId: '271acbf3-88b8-4a86-9964-29c2a202e85e'
    });
    // Retrieve the user token
    return oidcClient.getToken();
  }

  // Extract the context UUID from the querystring.
  var extractCtx = function() {
    return window.location.search.substr(
      window.location.search.indexOf('ctx=') + 4,
      36 // UUID is 36 chars long.
    );
  };

  var retrieveSlurmInfo = function() {
    var ctx = extractCtx();
    var token = authenticate();

    $.ajax('/sinfo', {
      headers: {
        Authorization: 'Bearer ' + token
      }})
      .done(function(data) {
        $('.error').empty();
        $('.sinfo-command').text(data.sinfoCommand);
        $('.sinfo-text').text(data.sinfoText);
        $('.sinfo-time').text(data.sinfoTime);
      })
      .fail(function(err) {
        console.log('failed');
        $('.error').text("Could not retrieve slurm info");
      });
  };

  init();
}());

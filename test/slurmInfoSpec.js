var sinon = require('sinon');
var rewire = require('rewire');
var expect = require('chai').expect;
var _ = require('lodash');
// rewire allows us to insert a mock for nexpect
var slurmInfo = rewire('../utils/slurmInfo');

var shellMock;
var nexpectMock;
var setIntervalMock;

var sampleInfo;

describe('SlurmInfo', function() {

  beforeEach(function(){

    shellMock = {
      run: function() {}
    };

    nexpectMock = {
      spawn: function() {}
    };

    sinon.stub(nexpectMock, "spawn", function () { return shellMock; });
    sinon.stub(shellMock, 'run');
    setIntervalMock = sinon.spy(function() {return 0; });

    slurmInfo.__set__({
      'nexpect': nexpectMock,
      'setInterval': setIntervalMock
    });

    sampleInfo = [
      'some weird error',
      'and another one',
      'PARTITION            | AVAIL | CPUS(A/I/O/T) | NODES(A/I/O/T) | GRES',
      'debug*               | up | 263/217/0/480 | 21/9/0/30  | (null)',
      'interactive          | up | 279/265/0/544 | 22/12/0/34 | TeslaC1060 gpu:42 SpiNNaKer:55',
      'test                 | up | 263/217/0/480 | 21/9/0/30  | LaserGun:14 not_a_GPU:45',
      'PARTITION            | NODES(A/I/)',
      'debug*               | 1/0',
      'interactive          | 2/3',
      'test                 | 4/2'
    ];

  });

  // sample result of sinfo with --format="%20P | %a | %C | %F"


  it('should start deamon', function() {
    slurmInfo.startFetchSinfo();

    // how to test the right function has been passed to setInterval?
    // expect(setIntervalMock.args[0][0]).to.equal(slurmInfo.sinfo);

    expect(setIntervalMock.calledOnce).to.equal(true);
  });

  it('should send sinfo command through ssh', function() {
    slurmInfo.sinfo();
    var command = nexpectMock.spawn.args[0][0];
    if (!_.isString(command)) {
      // called with array as command
      command = command.join(' ');
    }
    expect(command).to.contain('ssh').and
      .to.contain('bbpnrsoa@bbpviz1.cscs.ch').and
      .to.contain('sinfo').and
      .to.contain('"%20P | %5a | %15C | %15F | %G"');
  });

  it('should parse ssh output in run if ssh worked well', function() {
    sinon.spy(slurmInfo, 'parseInfo');
    expect(slurmInfo.latestSinfo).to.equal(null);
    slurmInfo.sinfo();
    expect(shellMock.run.calledOnce).to.equal(true);
    // run the callback passed to the run function
    var runCallback = shellMock.run.args[0][0];
    runCallback(null, sampleInfo, 0);
    expect(slurmInfo.parseInfo.calledOnce).to.equal(true);
    expect(slurmInfo.latestSinfo).not.to.equal(null);
    slurmInfo.parseInfo.restore();
  });

  it('should run callback with error if ssh failed', function() {
    sinon.spy(slurmInfo, 'parseInfo');
    var cbSpy = sinon.spy();
    var errorMock = {msg: 'some fake error'};
    slurmInfo.sinfo(cbSpy);
    // run the callback passed to the run function
    var runCallback = shellMock.run.args[0][0];
    runCallback(errorMock);

    expect(cbSpy.calledOnce).to.equal(true);
    expect(cbSpy.args[0][0]).to.equal(errorMock);
    expect(slurmInfo.latestSinfo).to.equal(null);
    slurmInfo.parseInfo.restore();
  });

  it('should run callback with parsed text if ssh succeeded', function() {
    sinon.spy(slurmInfo, 'parseInfo');
    var cbSpy = sinon.spy();
    slurmInfo.sinfo(cbSpy);
    // run the callback passed to the run function
    var runCallback = shellMock.run.args[0][0];
    runCallback(null, sampleInfo);

    expect(cbSpy.calledOnce).to.equal(true);
    expect(slurmInfo.latestSinfo).not.to.equal(null);
    expect(cbSpy.args[0][1]).to.equal(slurmInfo.latestSinfo);
    slurmInfo.parseInfo.restore();
  });


  it('should parse sinfo command', function() {
    var parsed = slurmInfo.parseInfo(sampleInfo);
    expect(parsed.length).to.equal(3);
    expect(parsed[1].name).to.equal('interactive');
    expect(parsed[1].nodes).to.deep.equal([22, 12, 0, 34]);
    expect(parsed[1].cpus).to.deep.equal([279, 265, 0, 544]);
    expect(parsed[1].gpus).to.equal(42);
    expect(parsed[1].free).to.equal(9); //12-3
  });

  it('should return 0 gpu when it is not in generic resources', function() {
    var parsed = slurmInfo.parseInfo(sampleInfo);
    expect(parsed[0].gpus).to.equal(0);
    expect(parsed[2].gpus).to.equal(0);
  });

  it('should throw an exception when no sinfo output is found', function() {
    var sshNoSinfo = 'some ssh error';
    expect(_.partial(slurmInfo.parseInfo,sshNoSinfo)).to.throw();
  });

});
